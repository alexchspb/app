https://argo-cd.readthedocs.io/en/stable/

# Argocd

## Initial setup

Note: also, we can install ArgoCD with helm https://github.com/argoproj/argo-helm/tree/main/charts/argo-cd

```bash
#> k create namespace argocd
#> k apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml

#% or core installation without some features
#> kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/core-install.yaml

#% Service Type to LB
#> k patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'


#% For UI accessing: https://localhost:8080/
#> k port-forward svc/argocd-server -n argocd 8080:443

#% Get creds (login: admin)
#> k get secrets/argocd-initial-admin-secret -n argocd -o json | jq .data.password
#> echo "<YOUR_SECRET>" | base64 -d
#% or with argocd (+ remove secret after it has been changed)
#> argocd admin initial-password -n argocd

#% Login to argocd from CLI using credentials above
#> argocd login --insecure --port-forward-namespace argocd localhost:8080

#% if you don't want port-forwarding each time, use:
#> export ARGOCD_OPTS='--insecure --port-forward-namespace argocd'

#> kubectl config get-contexts -o name
#> argocd cluster add minikube
```

## Deploy test app

```bash
#% Switch context to argocd
#> k config set-context --current --namespace=argocd

#% Show projects
#> argocd proj list

#% Create project
#> argocd proj create my-proj

#% Create app
#> k apply -f argoApp.yaml

#% Check pods
#> k get po

#% Check status
#> argocd app get my-app
#> argocd app list

#% Run deploy
#> argocd app sync my-app
```
