https://github.com/GoogleContainerTools/kaniko

```bash
#% Build locally
#> export CODE_VERSION=$(git rev-parse --short HEAD)
#> docker run -v ${HOME}/.docker/config.json:/kaniko/.docker/config.json -v ${PWD}:/workspace gcr.io/kaniko-project/executor:latest --dockerfile /workspace/Dockerfile --destination "registry.gitlab.com/alexchspb/app:sample" --context dir:///workspace/ --cache=false --push-retry=4 --image-fs-extract-retry 5
```
