# Skopeo 

https://github.com/containers/skopeo

## Installation

```bash
#> apt-get update
#> apt-get -y install skopeo
```

## Work with images

We'll work with DockerHub and GitLabRegitry

Pre-requisite: we already logged in into DockerHub and GitLabRegistry

```bash
#% Inspect image
#> skopeo inspect docker://registry.gitlab.com/alexchspb/app:f28c117
#> skopeo inspect --config docker://registry.gitlab.com/alexchspb/app:f28c117
#> skopeo inspect --config docker://registry.gitlab.com/alexchspb/app:f28c117| jq .history[].created

#% Copy image from GitLab to DockerHub
#> skopeo copy docker://registry.gitlab.com/alexchspb/app:f28c117 docker://lihong87/app:f28c117
#> skopeo copy docker://registry.gitlab.com/alexchspb/app:f28c117 docker://lihong87/core:f28c117
```
