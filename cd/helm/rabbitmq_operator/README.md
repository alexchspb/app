https://artifacthub.io/packages/helm/bitnami/rabbitmq-cluster-operator

# RabbitMQ operator

```bash
#> helm pull bitnami/rabbitmq-cluster-operator
#> helm install my-rabbit rabbitmq-cluster-operator/
#> kubectl get deploy -w --namespace default -l app.kubernetes.io/name=rabbitmq-cluster-operator,app.kubernetes.io/instance=my-rabbit

#% Sample cluster
#> k apply -f sample.yaml
```

## Non-operator

```bash
#> helm pull bitnami/rabbitmq
#> helm instal my-release rabbitmq/
```
