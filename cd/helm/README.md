# Helm

## Usefull commands

```bash
#% Add bitnami repo
#> helm repo add bitnami https://charts.bitnami.com/bitnami
#> helm repo list

#% Download chart
#> helm pull bitnami/keycloak
#> tar xzfp keycloak-13.1.3.tgz ; rm -rf keycloak-13.1.3.tgz

#% Linter
#> helm lint ./keycloak/

#% Dry run
#> helm install my-keycloak ./keycloak/ --dry-run --debug

#% Install chart
#> helm install my-keycloak ./keycloak/

#% Render template
#> helm template --debug ./keycloak/

#% Get manifest from installed release
#> helm get manifest my-keycloak

#% Get releases
#> helm list releasename --output yaml

#% One more way for installation
#> helm upgrade --install --wait --atomic --namespace keycloak --create-namespace --repo https://<...> <RELEASE_NAME> <CHART_NAME> --values - <<EOF
<SOME_CONFIG>
EOF
```

## Create and manage chart

```bash
#> helm create my-app
#> helm install my-app ./my-app
#> helm upgrade --install my-app ./my-app
#> helm upgrade --install my-app ./my-app -f ./my-app/dev_values.yaml
#> helm test my-app
#> helm ls
#> helm history my-app
#> helm upgrade my-app ./my-app
#> helm install -f ./my-app/dev_values.yaml my-app ./my-app

#> helm uninstall my-app
#> helm install --dry-run --debug --set someValue=test my-app ./my-app

#> helm list
#> helm get notes my-app
#> helm get values my-app
#> helm get manifest my-app
#> helm get hooks my-app
#> helm get all my-app
#> helm status my-app

#% Build dependencies
#> helm dependency build ./my-app

#% Update dependencies list 
#> helm dependency update ./my-app

#% Install with dependencies
#> helm install my-app ./my-app --set someVariable=test
```

## Kubectl

```bash
#> kubectl create deployment --image=nginx:1-alpine-slim my-app --dry-run=client -o yaml
```
