https://postgres-operator.readthedocs.io/en/latest/

# Installation

```bash
#% Add repo and pull
#> helm repo add postgres-operator-charts https://opensource.zalando.com/postgres-operator/charts/postgres-operator
#> helm repo add postgres-operator-ui-charts https://opensource.zalando.com/postgres-operator/charts/postgres-operator-ui
#> helm pull postgres-operator-charts/postgres-operator
#> helm pull postgres-operator-ui-charts/postgres-operator-ui

#> helm install postgres-operator ./postgres-operator
#> helm install postgres-operator-ui ./postgres-operator-ui

#% Verification
#> k get pod -l name=postgres-operator
#> k get pod -l app.kubernetes.io/name=postgres-operator
#> k logs "$(kubectl get pod -l name=postgres-operator --output='name')"

#% Make port-forwarding for UI
#> k port-forward svc/postgres-operator-ui 8081:80
#> curl http://localhost:8081/
```

## Deploy cluster with 1 (because minikube) instance

```bash
#% Deploy cluster
#> k apply -f cluster_example.yaml

#% Check services
#> k get svc acid-minimal-cluster

#% Make portforward for using with psql client
#> kubectl port-forward acid-minimal-cluster-0 5431:5432
#> telnet localhost 5431

#% Or
#> k exec -ti acid-minimal-cluster-0 bash
#> su postgres
#> psql
#> postgres=# select version();
```
