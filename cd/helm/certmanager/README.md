https://cert-manager.io/docs/installation/


# Certmanager

```bash
#% Add repo
#> helm repo add jetstack https://charts.jetstack.io ; helm repo update ; helm pull jetstack/cert-manager
#> helm install cert-manager ./cert-manager --namespace cert-manager --create-namespace --version v1.11.0 --set installCRDs=true
#> k get crd
```

```bash
#% Generating certificate
#> k apply -f selfsign.yaml
#> k apply -f ex.yaml
#> k get certificate -n sandbox
#> k get ClusterIssuer,Issuer -A
#> k get secrets -n sandbox -o yaml
```

## Verification

```bash
#% Validate the certificates against the CA
#> openssl verify -CAfile \
<(kubectl -n sandbox get secret root-secret \
  -o jsonpath='{.data.ca\.crt}' | base64 -d) \
<(kubectl -n sandbox get secret server-secret \
  -o jsonpath='{.data.tls\.crt}' | base64 -d)

#> openssl verify -CAfile \
<(kubectl -n sandbox get secret root-secret \
  -o jsonpath='{.data.ca\.crt}' | base64 -d) \
<(kubectl -n sandbox get secret client-secret \
  -o jsonpath='{.data.tls\.crt}' | base64 -d)
```

```bash
#% Validate client/server
#> touch test.txt

#> openssl s_server \
  -cert <(kubectl -n sandbox get secret server-secret -o jsonpath='{.data.tls\.crt}' | base64 -d) \
  -key <(kubectl -n sandbox get secret server-secret -o jsonpath='{.data.tls\.key}' | base64 -d) \
  -CAfile <(kubectl -n sandbox get secret root-secret -o jsonpath='{.data.ca\.crt}' | base64 -d) \
  -WWW -port 12345  \
  -verify_return_error -Verify 1 &

#> echo -e 'GET /test.txt HTTP/1.1\r\n\r\n' | \
  openssl s_client \
  -cert <(kubectl -n sandbox get secret client-secret -o jsonpath='{.data.tls\.crt}' | base64 -d) \
  -key <(kubectl -n sandbox get secret client-secret -o jsonpath='{.data.tls\.key}' | base64 -d) \
  -CAfile <(kubectl -n sandbox get secret client-secret -o jsonpath='{.data.ca\.crt}' | base64 -d) \
  -connect localhost:12345 -quiet

#> kill %1
```