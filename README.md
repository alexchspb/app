# Pre-requisites

```bash
#% Helm
#> wget https://get.helm.sh/helm-v3.9.0-linux-amd64.tar.gz
#> tar xzfp helm-v3.9.0-linux-amd64.tar.gz
#> chmod +x ./helm
#> mv helm /usr/local/bin/

#% Install helm diff plugin (will need for helmfile)
#> helm plugin install https://github.com/databus23/helm-diff

#% Helmfile
#> wget https://github.com/helmfile/helmfile/releases/download/v0.151.0/helmfile_0.151.0_linux_amd64.tar.gz
#> tar xzfp helmfile_0.151.0_linux_amd64.tar.gz
#> chown root: helmfile
#> chmod 555 helmfile
#> mv helmfile /usr/bin/

#% Argocd
#> wget https://github.com/argoproj/argo-cd/releases/download/v2.6.3/argocd-linux-amd64
#> mv argocd-linux-amd64 argocd
#> chown root: argocd
#> chmod 555 argocd
#> mv argocd /usr/bin/

#% Istioctl
#> wget https://github.com/istio/istio/releases/download/1.17.1/istio-1.17.1-linux-amd64.tar.gz
#> tar xzfp istio-1.17.1-linux-amd64.tar.gz
#> chown root: istio-1.17.1/bin/istioctl
#> chmod 555 istio-1.17.1/bin/istioctl
#> mv istio-1.17.1/bin/istioctl /usr/bin/

#% Kubectl
#> curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
#> chmod +x ./kubectl
#> mv ./kubectl /usr/local/bin/kubectl

#% Kubectx
#> git clone https://github.com/ahmetb/kubectx
#> mv kubectx .kubectx/
#> cd .kubectx/
#> mv kubectx kx
#> mv kubens kn

#% Minikube
#> curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && chmod +x minikube
#> install minikube /usr/local/bin/

#% For kvm2
#> apt update ; apt dist-upgrade -y ; apt install qemu qemu-kvm cpu-checker libvirt-clients libvirt-dev bridge-utils libvirt-daemon-system libvirt-daemon virtinst bridge-utils libosinfo-bin libguestfs-tools virt-top virt-manager skopeo ldap-utils -y
#> systemctl enable libvirtd
#> systemctl start libvirtd
#> usermod -aG libvirt $USER

#% Docker installation
#> apt remove docker docker-engine docker.io containerd runc
#> curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
#> echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
#> apt install docker-ce docker-ce-cli containerd.io docker-compose-plugin
#> curl -SL https://github.com/docker/compose/releases/download/v2.5.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
#> usermod -aG docker ${USER}

#% ... and reboot
#> reboot
```

```txt
#% Add following strings to the end of .bashrc
export PATH=~/.kubectx:$PATH
source <(kubectl completion bash)
alias k=kubectl
complete -F __start_kubectl k
```

## For certificate to inline transformation (within kubeconfig)

```bash
#> cat some_cert.crt | base64 -w0

#% or
#> cat some_cert.csr | base64 | tr -d '\n'
```

Also, you need replace such keys like `certificate-authority` in .kube/config file with `certificate-authority-data` (i.e. append `data` postfix to all keys, where you use inline certificate string instead certificate file path)

## Minikube management

Note! 2 separate clusters, within current lab, reasonable just for argocd and helmfile separate deployment testing (in the other words - phisical (not namespace) simulation `dev` and `prod` clusters)

```bash
#% Start single instance
#> minikube start --driver=kvm2

#% Stop and delete cluster
#> minikube stop ; minikube delete

#% Deploy cluster with 2 nodes
#> minikube start --nodes 2 --driver=kvm2

#% Cluster status
#> minikube status

#% Deploy multiple clusters
#> minikube start -p cluster-1
#> minikube start -p cluster-2

#% Switch between the clusters:
#% Switch to cluster-1
#> minikube profile cluster-1

#% Switch to cluster-2
#> minikube profile cluster-2

#% Get profile list
#> minikube profile list

#% Stop cluster, delete cluster
#> minikube stop -p cluster-1
#> minikube delete -p cluster-1

#% Minikube ip
#> minikube ip

#% Configure resources for minikube
#> minikube start --memory 8192 --cpus 4 --driver=kvm2
#> minikube stop
#> minikube config set memory 8192
#> minikube config set cpus 4
#> minikube start

#% Run for emulating external LB
#> minikube tunnel
```

## Working with minikube registry

```bash
#% Lets imagine, we build some image locally (outside of minikube)
#> docker build -t first-image -f ./Dockerfile .

#% Load local image to minikube
#> minikube image load first-image

#% Check images in minikube
#> minikube images ls --format table

#% Image using
#> k run first-container --image=first-image --image-pull-policy=Never --restart=Never
#> k run first-container --image=first-image --image-pull-policy=Never --restart=Never --dry-run=client -o yaml
```

```bash
#% This time, afer build image, it'll be stored exact in minikube (i.e. `docker` will run on the local machine, but with minikube's registry)
#% Use minikub's docker registry (use export variables of command from output)
#> minikube docker-env

#% Run docker images on your host before and after command exacution
#> eval $(minikube -p minikube docker-env)

#% Run image building ()
#> docker build -t second-image -f ./Dockerfile .

#% Check images in minikube
#> minikube images ls --format table

#% Image using
#> k run second-container --image=second-image --image-pull-policy=Never --restart=Never
```

```bash
#% Build directly on minikube
#> minikube image build -t third-image -f ./Dockerfile .

#% #% Image using
#> k run third-container --image=third-image --image-pull-policy=Never --restart=Never
```

## Minikube troubles

For some cases it resolve all external resources to 192.168.10.2. and we had No route to host error


```bash
#% Initial troubleshooting
#> minikube ssh -- ping -c 4 google.com
#> k get svc -n kube-system -o wide
```

```bash
#% The actual reason is
#> minikube ssh -- cat /etc/resolv.conf
search allza.ru
nameserver 192.168.10.2
options edns0 trust-ad ndots:0
```

The reason is Libvirt driver

```bash
#% As a workaround (but it's now working..)
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
```

```bash
#% But, we can do this (get ip address)
#> k get svc kube-dns -n kube-system

#> minikube ssh

#% Fix resolve.conf (replace nameserver ip with ip from previous step)
#> sudo vi /etc/resolv.conf

#> k -n kube-system rollout restart deployment coredns
#% Restart pod and check resolving again
```

# Manual work with application building and testing

```bash
#% Login to docker with creds
#> docker login registry.gitlab.com

#% Prepare short commit for using as app version
#> export CODE_VERSION=$(git rev-parse --short HEAD)

#% Build with parameters
#> docker build -t registry.gitlab.com/alexchspb/app:${CODE_VERSION} -f Dockerfile --build-arg CODE_VERSION=${CODE_VERSION} .

#% Docker HUB
#> docker build -t alexchspb/jft-app-py-0001:${CODE_VERSION} -f Dockerfile --build-arg CODE_VERSION=${CODE_VERSION} .

#% Push image to remote registry
#> docker push registry.gitlab.com/alexchspb/app:${CODE_VERSION}

#% Docker HUB
#> docker push alexchspb/jft-app-py-0001:${CODE_VERSION}

#% Run with port mapping (tcp/5000 is a flask app)
#> docker run --name=fl --rm -tid -p 80:5000 registry.gitlab.com/alexchspb/app:${CODE_VERSION}

#% or with variable passing
#> docker run --name=fl --rm -tid -p 80:5000 -e APP_NAME=flask_app -e APP_VERSION=v.0.1 registry.gitlab.com/alexchspb/app:${CODE_VERSION}

#% or with rewriting entrypoint
#> docker run --name=fl --rm -tid -p 80:5000 --entrypoint=/bin/bash -e APP_NAME=flask_app -e APP_VERSION=v.0.1 registry.gitlab.com/alexchspb/app:${CODE_VERSION}

#% Run for log checking in separate terminal
#> docker logs -f fl

#% Check it
#> curl http://localhost/ ; curl http://localhost/warn

#% Check version
#> docker exec -ti fl cat /app/version.txt

#% Go to container for troubleshooting
#> docker exec -ti fl bash 

#% Stop container (will be deleted, because `--rm` flag was passed during container running)
#> docker stop fl

#% Delete image
#> docker rmi registry.gitlab.com/alexchspb/app:${CODE_VERSION}
```

## Repushing images

```bash
#> docker container ls
#> docker image tag rhel-httpd:latest registry-host:5000/myadmin/rhel-httpd:latest
#> docker image push registry-host:5000/myadmin/rhel-httpd:latest
#> docker image ls

#> docker image tag myimage registry-host:5000/myname/myimage:latest
#> docker image tag myimage registry-host:5000/myname/myimage:v1.0.1
#> docker image tag myimage registry-host:5000/myname/myimage:v1.0
#> docker image tag myimage registry-host:5000/myname/myimage:v1
#> docker image ls

#> docker image push --all-tags registry-host:5000/myname/myimage
```

## Tiny images for testing

```bash
#% Public docker image (let's use 2 tags: 1-alpine-slim, 1.22-alpine)
#> docker pull nginx:${tag}
#> docker run --name nginx --rm -p 80:80 nginx:${tag}
#> curl http://localhost/

#% My test app (there are 2 tags for testing: 1f96cf43, 5327ce6f)
#> docker pull registry.gitlab.com/alexchspb/app:${tag}
#> docker run --name myapp --rm -p 80:5000 app:${tag}
#> curl http://localhost/

#% Re-tag
#> docker tag local-image:tagname new-repo:tagname
#> docker push new-repo:tagname
```

## Kubectl snippets

```bash
#> k create service nodeport ns-service --tcp=80:80 --dry-run=client -o yaml
#> k expose pod nginx --port=80 --target-port=8000 --dry-run=client -o yaml
#> k create deployment --image=nginx:1-alpine-slim my_app --dry-run=client -o yaml
```
