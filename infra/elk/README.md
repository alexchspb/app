# Install ELK with docker compose

```bash
#% Create for data storing
#> mkdir -p /storage/volume/elk/data

#% Installing with docker compose
#> docker-compose up -d

#% Show ports for accessing
#> docker-compose ps -a

#% Stop docker-compose
#> docker-compose down
```
