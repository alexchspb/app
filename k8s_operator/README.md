# Kubernetes operator

## Go installing

```bash
#% Check required GO version, for example
#> operator-sdk version

#> wget https://go.dev/dl/go1.20.2.linux-amd64.tar.gz
#> tar -xvf go1.20.2.linux-amd64.tar.gz 
#> mv go /usr/local 

#% All the bellow environment will be set for your current session only. To make it permanent add above commands in ~/.profile file.
#> export GOROOT=/usr/local/go
#> export GOPATH=/storage/go/tmp/
#> export PATH=$GOPATH/bin:$GOROOT/bin:$PATH

#>  go version
```

## Operator SDK

https://sdk.operatorframework.io/docs/installation/

```bash
#> export ARCH=$(case $(uname -m) in x86_64) echo -n amd64 ;; aarch64) echo -n arm64 ;; *) echo -n $(uname -m) ;; esac)
#> export OS=$(uname | awk '{print tolower($0)}')
#> export OPERATOR_SDK_DL_URL=https://github.com/operator-framework/operator-sdk/releases/download/v1.27.0
#> curl -LO ${OPERATOR_SDK_DL_URL}/operator-sdk_${OS}_${ARCH}

#> gpg --keyserver keyserver.ubuntu.com --recv-keys 052996E2A20B5C7E
#> curl -LO ${OPERATOR_SDK_DL_URL}/checksums.txt
#> curl -LO ${OPERATOR_SDK_DL_URL}/checksums.txt.asc
#> gpg -u "Operator SDK (release) <cncf-operator-sdk@cncf.io>" --verify checksums.txt.asc

#> grep operator-sdk_${OS}_${ARCH} checksums.txt | sha256sum -c -
#> chmod +x operator-sdk_${OS}_${ARCH} && sudo mv operator-sdk_${OS}_${ARCH} /usr/local/bin/operator-sdk
```

## Kubebuilder

https://book.kubebuilder.io/quick-start.html

```bash
#> curl -L -o kubebuilder https://go.kubebuilder.io/dl/latest/$(go env GOOS)/$(go env GOARCH)
#> chmod +x kubebuilder && mv kubebuilder /usr/local/bin/
```


https://anupamgogoi.medium.com/writing-a-kubernetes-operator-from-zero-to-hero-8ca5dc2462b7

## Operator-SDK project initialization example

```bash
#% Check current api-versions
#> k api-versions

#% Check CRDs
#> k get crd

#% Generate operator project
#> operator-sdk init --domain alexchspb.ru --repo github.com/alexchspb/jft-demo-k8s-operator-sdk-0001
#> go get sigs.k8s.io/controller-runtime@v0.13.0

#% Update dependencies
#> go mod tidy

#% Create the API and the Controller
#> operator-sdk create api --group apps --version v1 --kind JftDemoOperatorSDK --resource --controller

#% Generate CRDs
#> tree config/samples
#> cat config/samples/apps_v1_jftdemooperatorsdk.yaml

#% Generating CRDs
#> make generate
#> make manifests

#% Install CRD
#> make install

#% or
#> k apply -f config/crd/bases/*.yaml
```

- `Makefile` - commands to generate the artifacr for operator. `make help` will show useful commands;
- `main.go` - central point of entry to operator, contains main function;
- `controllers/jftdemooperatorsdk_controller.go` - The controller. The main logic of the operator goes here;
- `api/v1/jftdemooperatorsdk_types.go` - CR's structure
- `api/v1/groupversion_info.go` - group and version info we specified during operator creation

```yaml
#% config/samples/apps_v1_jftdemooperatorsdk.yaml
apiVersion: apps.alexchspb.ru/v1
kind: JftDemoOperatorSDK
metadata:
  labels:
    app.kubernetes.io/name: jftdemooperatorsdk
    app.kubernetes.io/instance: jftdemooperatorsdk-sample
    app.kubernetes.io/part-of: jft-demo-k8s-operator-sdk-0001
    app.kubernetes.io/managed-by: kustomize
    app.kubernetes.io/created-by: jft-demo-k8s-operator-sdk-0001
  name: jftdemooperatorsdk-sample
spec:
  image: "alexchspb/jft-app-py-0001:930d1750"
  size: 1
```

```go
//% api/v1/jftdemooperatorsdk_types.go
/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// JftDemoOperatorSDKSpec defines the desired state of JftDemoOperatorSDK
type JftDemoOperatorSDKSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Foo is an example field of JftDemoOperatorSDK. Edit jftdemooperatorsdk_types.go to remove/update
	Image string `json:"image,omitempty"`
	Size  int32  `json:"size"`
}

// JftDemoOperatorSDKStatus defines the observed state of JftDemoOperatorSDK
type JftDemoOperatorSDKStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
}

//+kubebuilder:object:root=true
//+kubebuilder:subresource:status

// JftDemoOperatorSDK is the Schema for the jftdemooperatorsdks API
type JftDemoOperatorSDK struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   JftDemoOperatorSDKSpec   `json:"spec,omitempty"`
	Status JftDemoOperatorSDKStatus `json:"status,omitempty"`
}

//+kubebuilder:object:root=true

// JftDemoOperatorSDKList contains a list of JftDemoOperatorSDK
type JftDemoOperatorSDKList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []JftDemoOperatorSDK `json:"items"`
}

func init() {
	SchemeBuilder.Register(&JftDemoOperatorSDK{}, &JftDemoOperatorSDKList{})
}
```

```go
//% controllers/jftdemooperatorsdk_controller.go
/*
Copyright 2023.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"

	a "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/log"

	appsv1 "github.com/alexchspb/jft-demo-k8s-operator-sdk-0001/api/v1"
)

// JftDemoOperatorSDKReconciler reconciles a JftDemoOperatorSDK object
type JftDemoOperatorSDKReconciler struct {
	client.Client
	Scheme *runtime.Scheme
}

//+kubebuilder:rbac:groups=apps.alexchspb.ru,resources=jftdemooperatorsdks,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=apps.alexchspb.ru,resources=jftdemooperatorsdks/status,verbs=get;update;patch
//+kubebuilder:rbac:groups=apps.alexchspb.ru,resources=jftdemooperatorsdks/finalizers,verbs=update

// Reconcile is part of the main kubernetes reconciliation loop which aims to
// move the current state of the cluster closer to the desired state.
// TODO(user): Modify the Reconcile function to compare the state specified by
// the JftDemoOperatorSDK object against the actual cluster state, and then
// perform operations to make the cluster state reflect the state specified by
// the user.
//
// For more details, check Reconcile and its Result here:
// - https://pkg.go.dev/sigs.k8s.io/controller-runtime@v0.13.0/pkg/reconcile
func (r *JftDemoOperatorSDKReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	_ = log.FromContext(ctx)

	jftDemoOperatorSDK := &appsv1.JftDemoOperatorSDK{}
	err := r.Client.Get(ctx, req.NamespacedName, jftDemoOperatorSDK)
	if err != nil {
		if errors.IsNotFound(err) {

			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}

	found := &a.Deployment{}
	err = r.Client.Get(ctx, types.NamespacedName{Name: jftDemoOperatorSDK.Name, Namespace: jftDemoOperatorSDK.Namespace}, found)
	if err != nil && errors.IsNotFound(err) {
		dep := r.deployJftDemoOperatorSDK(jftDemoOperatorSDK)
		err = r.Client.Create(ctx, dep)
		if err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{Requeue: true}, nil
	} else if err != nil {
		return ctrl.Result{}, err
	}

	size := jftDemoOperatorSDK.Spec.Size
	if *found.Spec.Replicas != size {
		found.Spec.Replicas = &size
		err = r.Client.Update(ctx, found)
		if err != nil {
			return ctrl.Result{}, err
		}
		return ctrl.Result{Requeue: true}, nil
	}

	return ctrl.Result{}, nil
}

// SetupWithManager sets up the controller with the Manager.
func (r *JftDemoOperatorSDKReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&appsv1.JftDemoOperatorSDK{}).
		Complete(r)
}

func (c *JftDemoOperatorSDKReconciler) deployJftDemoOperatorSDK(ha *appsv1.JftDemoOperatorSDK) *a.Deployment {
	replicas := ha.Spec.Size
	labels := map[string]string{"app": "busybox"}
	image := ha.Spec.Image
	dep := &a.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      ha.Name,
			Namespace: ha.Namespace,
		},
		Spec: a.DeploymentSpec{
			Replicas: &replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{{
						Image: image,
						Name:  ha.Name,
					}},
				},
			},
		},
	}
	ctrl.SetControllerReference(ha, dep, c.Scheme)
	return dep
}
```

```go
//% main.go
package main

import (
	"flag"
	"os"

	_ "k8s.io/client-go/plugin/pkg/client/auth"

	"k8s.io/apimachinery/pkg/runtime"
	utilruntime "k8s.io/apimachinery/pkg/util/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/healthz"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	appsv1 "github.com/alexchspb/jft-demo-k8s-operator-sdk-0001/api/v1"
	"github.com/alexchspb/jft-demo-k8s-operator-sdk-0001/controllers"
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

func init() {
	utilruntime.Must(clientgoscheme.AddToScheme(scheme))

	utilruntime.Must(appsv1.AddToScheme(scheme))
}

func main() {
	var metricsAddr string
	var enableLeaderElection bool
	var probeAddr string
	flag.StringVar(&metricsAddr, "metrics-bind-address", ":8080", "The address the metric endpoint binds to.")
	flag.StringVar(&probeAddr, "health-probe-bind-address", ":8081", "The address the probe endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "leader-elect", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")
	opts := zap.Options{
		Development: true,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:                 scheme,
		MetricsBindAddress:     metricsAddr,
		Port:                   9443,
		HealthProbeBindAddress: probeAddr,
		LeaderElection:         enableLeaderElection,
		LeaderElectionID:       "1b84a572.alexchspb.ru",
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	if err = (&controllers.JftDemoOperatorSDKReconciler{
		Client: mgr.GetClient(),
		Scheme: mgr.GetScheme(),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "JftDemoOperatorSDK")
		os.Exit(1)
	}

	if err := mgr.AddHealthzCheck("healthz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up health check")
		os.Exit(1)
	}
	if err := mgr.AddReadyzCheck("readyz", healthz.Ping); err != nil {
		setupLog.Error(err, "unable to set up ready check")
		os.Exit(1)
	}

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
```

```bash
#% We have to download `log` module separatelly, because it was deleted from github, but exist in previous tags
#> go get github.com/prometheus/common/log@v0.26.0
#> go get github.com/go-logr/logr@v1.1.0
#> go mod tidy
```

### Run operator locally

```bash
#> go run main.go

#% Clean go.mod
#> go mod tidy
```

```bash
#> k create ns test
#> k apply -f ./config/samples/apps_v1_jftdemooperatorsdk.yaml -n test
#> k get all -n test
#> k get jftdemooperatorsdk -n test
#> k get po -n test -w
```

### Make and push docker image

```bash
#> make docker-build IMG=alexchspb/jft-demo-k8s-operator-sdk-0001:latest
#> make docker-push IMG=alexchspb/jft-demo-k8s-operator-sdk-0001:latest
```
