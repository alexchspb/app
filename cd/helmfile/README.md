# Helmfile

## Pre-requisites

As frontend we'll use: `nginx:${tag} docker image with tags {1-alpine-slim, 1.22-alpine}` and local helm chart `./cd/helm/my-app/`.

As backend: `registry.gitlab.com/alexchspb/app:${tag} docker image with tags {1f96cf43, 5327ce6f}` and local helm chart `./cd/helm/my-app-py/`.


## Deploy app to different envs

```bash
#> k create ns dev
#> helmfile -n dev -e dev apply
#> k get po -n dev --watch
#> helmfile -n dev -e dev destroy


#> k create ns prod
#> helmfile -n prod -e prod apply
#> k get po -n prod --watch
#> helmfile -n prod -e prod destroy --interactive
```
