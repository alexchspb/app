```bash
docker run -d --name=nginx --rm -e APP_NAME=ttt -p 80:80 -v ${PWD}/nginx.conf:/usr/local/openresty/nginx/conf/nginx.conf openresty/openresty:alpine
curl http://localhost/
docker stop nginx
```

```config
pcre_jit on;

events {
    worker_connections  1024;
}

env APP_NAME;

http {
    include       mime.types;
    default_type  application/octet-stream;

    client_body_temp_path /var/run/openresty/nginx-client-body;
    proxy_temp_path       /var/run/openresty/nginx-proxy;
    fastcgi_temp_path     /var/run/openresty/nginx-fastcgi;
    uwsgi_temp_path       /var/run/openresty/nginx-uwsgi;
    scgi_temp_path        /var/run/openresty/nginx-scgi;

    sendfile        on;
    keepalive_timeout  65;

    server {
        listen 80 default_server;
        location / {
        set_by_lua $app_name 'return os.getenv("APP_NAME")';
        types {} default_type text/html; return 200 '$app_name';
        }
    }
}
```
