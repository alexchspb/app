
# OpenLDAP

```bash
#> git clone git@github.com:TalkingQuickly/kubernetes-sso-guide.git
#> helm upgrade --install openldap ./kubernetes-sso-guide/charts/openldap --values values.yaml
#> k get secret --namespace default openldap -o jsonpath="{.data.LDAP_ADMIN_PASSWORD}" | base64 --decode; echo
#> k get secret --namespace default openldap -o jsonpath="{.data.LDAP_CONFIG_PASSWORD}" | base64 --decode; echo

#> kubectl port-forward --namespace default \
      $(kubectl get pods -n default --selector='release=openldap' -o jsonpath='{.items[0].metadata.name}') \
      3890:389

#% Admin password and config password aren't the same
#> ldapsearch -x -H ldap://localhost:3890 -b dc=sso,dc=allza,dc=ru -D "cn=admin,dc=sso,dc=allza,dc=ru" -w admin
```
