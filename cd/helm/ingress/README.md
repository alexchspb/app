

# Ingress

```bash
#> helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
#> helm repo update
#> helm pull ingress-nginx/ingress-nginx
#> helm install ingress-nginx ./ingress-nginx
```

```bash
#% k8s_operators/ingress/kubernetes-ingress/deployments/
#> git clone https://github.com/nginxinc/kubernetes-ingress.git --branch v3.0.2
#> cd kubernetes-ingress/deployments
#> k apply -f common/ns-and-sa.yaml
#> k apply -f rbac/rbac.yaml
#> k apply -f common/default-server-secret.yaml
#> k apply -f common/nginx-config.yaml
#> k apply -f common/ingress-class.yaml
#> k apply -f common/crds/k8s.nginx.org_virtualservers.yaml
#> k apply -f common/crds/k8s.nginx.org_virtualserverroutes.yaml
#> k apply -f common/crds/k8s.nginx.org_transportservers.yaml
#> k apply -f common/crds/k8s.nginx.org_policies.yaml
#> k apply -f common/crds/k8s.nginx.org_globalconfigurations.yaml
#> k apply -f deployment/nginx-ingress.yaml
#> k apply -f service/nodeport.yaml
#> k get svc -A

#> k describe svc nginx-ingress -n nginx-ingress
#> k get pods -n nginx-ingress
#> k get crd -A
```
