FROM python:3.7.16-slim-buster
COPY ./src /app
WORKDIR /app
ARG CODE_VERSION=${CODE_VERSION}
RUN echo ${CODE_VERSION} > version.txt
RUN pip3 install -r requirements.txt
ENTRYPOINT ["python3"]
CMD ["app.py"]
