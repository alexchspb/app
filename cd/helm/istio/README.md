https://istio.io/latest/docs/setup/install/istioctl/

# Istio
## Installation with istioctl

```bash
#> istioctl profile list
#> istioctl install --set profile=default
#> k -n istio-system get deploy
#> k get crd
#> k get gw,vs,ingress -A

#% Make pod injections for default ns
#> k label namespace default istio-injection=enabled

#% Deploy application and istio objects
#> cd ../my-app-py/
#> helm install my-app-py .
#> k apply -f gw_ex.yaml 
#> k apply -f virt_svc.yaml 
#> kubectl get svc istio-ingressgateway -n istio-system

#% Run for emulating LB
#> minikube tunnel

#> curl http://<LB_IP>:<ISTIO_GW_PORT>
```
