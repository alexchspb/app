# Prometheus

```bash
#% Make directories for data
#> mkdir -p /storage/volume/{prometheus,grafana,alertmanager,blackbox}/data

#% Installing with docker compose
#> docker-compose up -d

#% Show ports for accessing
#> docker-compose ps -a

#% Stop docker-compose
#> docker-compose down
```
