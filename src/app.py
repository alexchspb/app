#!/usr/bin/env python3
import os, sys
from logging.config import dictConfig
from flask import Flask
from flask_restful import Resource, Api

dictConfig(
    {
        "version": 1,
        "formatters": {
            "default": {
                "format": "[%(asctime)s] %(levelname)s in %(module)s: %(message)s",
            }
        },
        "handlers": {
            "console": {
                "class": "logging.StreamHandler",
                "stream": "ext://sys.stdout",
                "formatter": "default",
            },
            "size-rotate": {
                "class": "logging.handlers.RotatingFileHandler",
                "filename": "flask.log",
                "maxBytes": 1000000,
                "backupCount": 5,
                "formatter": "default",
            },
        },
        "root": {"level": "DEBUG", "handlers": ["console", "size-rotate"]},
    }
)


HOST = os.getenv('HOSTNAME')
APP_NAME = os.getenv('APP_NAME')
APP_VERSION = os.getenv('APP_VERSION')
CODE_VERSION = open('/app/version.txt', 'r')

app = Flask(__name__)
api = Api(app)

class Root(Resource):
    def get(self):
        return {'HOSTNAME': HOST, 'APP_NAME': APP_NAME, 'APP_VERSION': APP_VERSION, 'CODE_VERSION': CODE_VERSION.read().strip("\n")}
    
class Status(Resource):
    def get(self):
        print('STDOUT_SAMPLE_PRINT', file=sys.stdout)
        return {'STATUS': 'Online'}

@app.route("/debug")
def debug():
    app.logger.info('Print: debug log')
    return "Hello, World! (debug)"

@app.route("/info")
def info():
    app.logger.info('Print: info log')
    return "Hello, World! (info)"

@app.route("/warn")
def warn():
    app.logger.warning('Print: warning log')
    return "Hello, World! (warn)"

@app.route("/error")
def error():
    app.logger.error('Print: error log')
    return "Hello, World! (error)"

@app.route("/critical")
def critical():
    app.logger.critical('Print: critical log')
    return "Hello, World! (critical)"

api.add_resource(Root, '/')
api.add_resource(Status, '/status')

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
