# Flask
flask==2.2.3
flask_restful==0.3.9
# RabbitMQ
pika==1.3.1
# PostgreSQL
psycopg2-binary
# psycopg2==2.9.5
# Vault
hvac==1.0.2
