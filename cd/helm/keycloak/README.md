https://www.keycloak.org/

# Keycloak

```bash
#> helm upgrade --install --wait --atomic --namespace keycloak --create-namespace --repo https://codecentric.github.io/helm-charts keycloak keycloak --values - <<EOF
extraEnv: |
  - name: KEYCLOAK_USER
    value: admin
  - name: KEYCLOAK_PASSWORD
    value: admin
ingress:
  enabled: true
  annotations:
    kubernetes.io/ingress.class: nginx
  rules:
    - host: keycloak.kind.cluster
      paths:
        - path: /
          pathType: Prefix
  tls: null
EOF
```

```bash
#> k get all,svc -n keycloak

#% Make port-forwarding for access outside
#> k -n keycloak port-forward svc/keycloak-http 8080:80

#> curl http://localhost:8080/
```